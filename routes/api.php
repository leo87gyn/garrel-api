<?php

Route::group(['middleware' => 'jwt.auth'], function(){
  Route::resource('produtosErp', 'ProdutosErpController');
  Route::resource('clientesErp', 'ClientesErpController');
  Route::resource('venda', 'ProdutosVendaController');
  Route::resource('vendas', 'VendaController');
  Route::resource('categorias', 'CategoriaController');
  Route::resource('formasPagamento', 'FormasDePagamentoController');
  Route::resource('produto', 'ProdutoController');
  Route::resource('tiposDePagamento', 'TiposPagamentoController');
  Route::resource('empresas', 'EmpresasController');
  Route::get('/gerarPdf', 'VendaController@gerarPdf');
  Route::post('/enviarEmail', 'ProdutosVendaController@enviarEmail');
  Route::get('/verificarCategoria', 'CategoriaController@verificarCategoria');
  Route::get('/verificarProduto', 'ProdutoController@verificarProduto');
  Route::get('/verificarFormasDePagamento', 'FormasDePagamentoController@verificarFormasDePagamento');

});

Route::post('logar', 'AuthApiController@autenticar');
