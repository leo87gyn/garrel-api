<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientesErp extends Model
{
  /*
    Listagem resumida dos clientes
  */
    public function mostrarClientesErp(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://app.omie.com.br/api/v1/geral/clientes/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ListarClientesResumido\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"pagina\":1,\n\t\t\t\"registros_por_pagina\":100,\n\t\t\t\"apenas_importado_api\":\"N\"\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: 1433ec5c-145f-488b-ae55-93d86a7455b2"
        ),
      ));

      $response = curl_exec($curl);

      $curlPaginaDois = curl_init();

      curl_setopt_array($curlPaginaDois, array(
        CURLOPT_URL => "https://app.omie.com.br/api/v1/geral/clientes/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ListarClientesResumido\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"pagina\":2,\n\t\t\t\"registros_por_pagina\":100,\n\t\t\t\"apenas_importado_api\":\"N\"\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: 1a442489-1bf4-4635-856b-dc6143ec759b",
          "apenas_importado_api: N",
          "app_key: 480492519507"
        ),
      ));

      $responsePaginaDois = curl_exec($curlPaginaDois);


      $err = curl_error($curl);
      $errPaginaDois = curl_error($curlPaginaDois);

      curl_close($curl);
      curl_close($curlPaginaDois);




      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        //echo  $response ;
        $json = json_decode($response, true);
        $jsonPaginaDois = json_decode($responsePaginaDois, true);
        $result = array_merge($json['clientes_cadastro_resumido'],$jsonPaginaDois['clientes_cadastro_resumido']);
        $json['clientes_cadastro_resumido'] = $result;

        return $json;
        }
      }

      public function retornaClientePorCodigo($cliente){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://app.omie.com.br/api/v1/geral/clientes/",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ConsultarCliente\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"codigo_cliente_omie\":\"$cliente\",\n\t\t\t\"codigo_cliente_integracao\":\"\"\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 300d19be-4ab5-476f-acf0-8b2ac54452db",
            "apenas_importado_api: N",
            "app_key: 480492519507",
            "app_secret: 8d3aa8cf9538a9179193ad4a654efb09",
            "call: ListarClientes",
            "pagina: 1",
            "registros_por_pagina: 50"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          //echo  $response ;
          $cliente=array();
          $json = json_decode($response, true);
          $cliente[0]['razao_social']=$json['razao_social'];
          $cliente[0]['endereco']=$json['cidade']." ".$json['endereco']." ".$json['bairro']." ".$json['complemento'];
          $cliente[0]['cnpj_cpf']=$json['cnpj_cpf'];

          return $cliente;
          }
      }
}
