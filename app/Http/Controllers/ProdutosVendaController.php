<?php

namespace App\Http\Controllers;
use App\ProdutosVenda;
use App\Venda;
use App\ProdutosErp;
use App\ClientesErp;
use Illuminate\Http\Request;

class ProdutosVendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = \JWTAuth::toUser($request->token);
      $total = 0;
      $produtos = $request->venda;
      $fornecedor_id  = $produtos[0]['fornecedor'];
      $pagamento_id = $produtos[0]['pagamento_id'];


      $produtosErp = new ClientesErp();
      $cliente = $produtosErp->retornaClientePorCodigo($produtos[0]['cliente']);
      $cliente_nome=$cliente[0]['razao_social'];
      $cnpj_cpf = $cliente[0]['cnpj_cpf'];
      $endereco = $cliente[0]['endereco'];

      $endereco_formatado = str_replace(',' , '-' , $endereco);
      $total =0;
      foreach ($produtos as $produto) {
        $total += $produto['total'];
      }

      //return response()->json($endereco);


      $venda = new Venda();
      $venda->cliente_omie_id=$produtos[0]['cliente'];
      $venda->user_id=$user->id;
      $venda->total = $total;
      $venda->cliente_nome = $cliente_nome;
      $venda->endereco=str_replace(',','-',$endereco);
      $venda->uuid = \Uuid::generate()->string;
      $venda->cnpj=$cnpj_cpf;
      $venda->tipo_pagamento_omie_id=$produtos[0]['pagamento_id'];
      $venda->tipo_pagamento='vazio';
      $venda->tipo_pagamento_id = $pagamento_id;
      $venda->save();


      foreach ($produtos as $produto) {
        $produtosVenda = new ProdutosVenda();
        $produtosVenda->produto_omie_id=$produto['carne'];
        $produtosVenda->venda_id=$venda->id;
        $produtosVenda->preco=$produto['preco'];
        $produtosVenda->produto_id=$produto['carne'];
        $produtosVenda->fornecedor_id=$produto['fornecedor'];
        $produtosVenda->quantidade=$produto['quantidade'];

        $produtosErp = new ProdutosErp();
        $carne = $produtosErp->retornaProdutoPorCodigo($produto['carne']);

        $produtosVenda->produto_nome = $carne;
        $produtosVenda->save();
      }





      return response()->json('sucess',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdutosVenda  $produtosVenda
     * @return \Illuminate\Http\Response
     */
    public function show(ProdutosVenda $produtosVenda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdutosVenda  $produtosVenda
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdutosVenda $produtosVenda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdutosVenda  $produtosVenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdutosVenda $produtosVenda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdutosVenda  $produtosVenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdutosVenda $produtosVenda)
    {
        //
    }
    public function enviarEmail(Request $request){
      $venda_id = $request->venda_id;
      $venda = new Venda();
      $chave = Venda::with(['produtosVenda','user','formas_pagamento'])
      ->where('id',$venda_id)
      ->first();
      $email="belini.repre@gmail.com";
      $vendedor="Paulo César";
      $cliente_nome=$chave->cliente_nome;
      $data_venda = $chave->created_at;
      $data_venda = date('d-m-Y h:i:s', strtotime($data_venda));
      $total_geral = $chave->total;
      $endereco = $chave->endereco;
      $uuid = $chave->id;
        $html='';
        $html.="<table width='100%' border='4' >
        <thead  align='center' style='display: table-header-group'>
          <tr>
            <th class='center' width='50%' >
                 Descrição do produto
            </th>
            <th class='center' >
                Quantidade(Kg)
            </th>
            <th class='center' >
                Preco(Kg)
            </th>
            <th class='center' >
              <span>  Total </span>
            </th>
          </tr>
          </thead>
          ";
          //return response()->json($chave->produtosVenda);

          foreach ($chave->produtosVenda as $produto) {
            $html.='<tr style="text-align:center"><td class="fonte2">'.$produto->produto_nome.'</td>
            <td  class="fonte2" style="text-align:center" >'.$produto->quantidade.'</td>
            <td  class="fonte2"  style="text-align:center">'.number_format($produto->preco,2).'</td>
            <td  class="fonte2" style="text-align:center">'.number_format($produto->quantidade*$produto->preco,2).'</td>
            </tr>';
          }

          $email_envio=$request->email;


          \Mail::send('emails.send', ['cliente_nome' => $cliente_nome , 'data_venda' => $data_venda , 'endereco'=> $endereco , 'vendedor'=>$vendedor , 'produtos'=>$chave->produtosVenda , 'total_geral'=>$total_geral], function ($message) use ($email_envio)
            {
              $message->from('leo87gyn@gmail.com', 'Dados da venda');
              $message->subject('Bem Vindo');
              $message->to($email_envio);

            });



      return response()->json('sucess',200);
    }
}
