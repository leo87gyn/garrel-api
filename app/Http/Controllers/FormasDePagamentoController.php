<?php

namespace App\Http\Controllers;

use App\FormasDePagamento;
use Illuminate\Http\Request;

class FormasDePagamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , FormasDePagamento $FormasDePagamento)
    {
      if($request->combo=="select"){
        return response()->json($FormasDePagamento::all());
      }
        $chave = $FormasDePagamento::paginate(15);
        return response()->json($chave);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , FormasDePagamento $FormasDePagamento)
    {

      $FormasDePagamento->nome = $request->nome;
      $FormasDePagamento->save();

      return $retorno=[
        'mensagem'=>'Inserido com Sucesso',
        'success'=>true,
        'status'=>200
      ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormasDePagamento  $FormasDePagamento
     * @return \Illuminate\Http\Response
     */
    public function show(FormasDePagamento $FormasDePagamento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormasDePagamento  $FormasDePagamento
     * @return \Illuminate\Http\Response
     */
    public function edit(FormasDePagamento $FormasDePagamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormasDePagamento  $FormasDePagamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormasDePagamento $FormasDePagamento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormasDePagamento  $FormasDePagamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $FormasDePagamento = new FormasDePagamento();
      $FormasDePagamentoSelecionada=$FormasDePagamento->find($request->id);
      $FormasDePagamentoSelecionada->delete();
      return response()->json($FormasDePagamentoSelecionada);

    }

    public function verificarFormasDePagamento(Request $request , FormasDePagamento $FormasDePagamento){
      $existeFormasDePagamento = $FormasDePagamento::where('nome','=',$request->nome)->withTrashed()->first();
      if($existeFormasDePagamento && $request->id == $existeFormasDePagamento->id){
        return response()->json(true);
      }
      if(!$existeFormasDePagamento){
        return response()->json(true);
      }
      else{
        return response()->json(false);
      }
    }
}
