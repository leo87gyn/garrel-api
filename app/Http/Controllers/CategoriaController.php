<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , Categoria $categoria)
    {
      if($request->combo=="select"){
        return response()->json($categoria::all());
      }
        $chave = $categoria::paginate(15);
        return response()->json($chave);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , Categoria $categoria)
    {

      $categoria->nome = $request->nome;
      $categoria->save();

      return $retorno=[
        'mensagem'=>'Inserido com Sucesso',
        'success'=>true,
        'status'=>200
      ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $categoria = new Categoria();
      $categoriaSelecionada=$categoria->find($request->id);
      $categoriaSelecionada->delete();
      return response()->json($categoriaSelecionada);

    }

    public function verificarCategoria(Request $request , Categoria $categoria){
      $existeCategoria = $categoria::where('nome','=',$request->nome)->withTrashed()->first();
      if($existeCategoria && $request->id == $existeCategoria->id){
        return response()->json(true);
      }
      if(!$existeCategoria){
        return response()->json(true);
      }
      else{
        return response()->json(false);
      }
    }
}
