<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , Produto $Produto)
    {

      if($request->combo=="select"){
        return response()->json($Produto::all());
      }
      $chave = Produto::with('categoria_id')
      ->paginate(50);
      return response()->json($chave);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , Produto $Produto)
    {

      $Produto->nome = $request->nome;
      $Produto->categorias_id = $request->categoria_id;
      $Produto->preco = $request->preco;
      $Produto->save();

      return $retorno=[
        'mensagem'=>'Inserido com Sucesso',
        'success'=>true,
        'status'=>200
      ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produto  $Produto
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $Produto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produto  $Produto
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $Produto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produto  $Produto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produto $Produto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produto  $Produto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $Produto = new Produto();
      $ProdutoSelecionada=$Produto->find($request->id);
      $ProdutoSelecionada->delete();
      return response()->json($ProdutoSelecionada);

    }

    public function verificarProduto(Request $request , Produto $Produto){
      $existeProduto = $Produto::where('nome','=',$request->nome)->withTrashed()->first();
      if($existeProduto && $request->id == $existeProduto->id){
        return response()->json(true);
      }
      if(!$existeProduto){
        return response()->json(true);
      }
      else{
        return response()->json(false);
      }
    }
}
