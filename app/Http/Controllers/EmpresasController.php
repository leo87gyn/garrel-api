<?php

namespace App\Http\Controllers;

use App\Empresas;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , Empresas $empresas)
    {

      if($request->combo=="select"){
        return response()->json($empresas::all());
      }
        $chave = $empresas::paginate(15);
        return response()->json($chave);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , Empresas $empresas)
    {

        $verificarEmpresaExiste = $empresas::where('razao_social', '=' ,$request->formulario['razao_social'])->first();
        if($verificarEmpresaExiste){
          return response()->json([
            'success'=>false,
            'mensagem'=>'Empresa já existe , escolha outro nome'
          ]);
        }
        else{
          $empresa = new Empresas();
          $empresa->razao_social = $request->formulario['razao_social'];
          $empresa->fantasia = $request->formulario['fantasia'];
          $empresa->proprietario = $request->formulario['proprietario'];
          $empresa->email = $request->formulario['email'];
          $empresa->endereco = $request->formulario['endereco'];
          $empresa->telefone = $request->formulario['telefone'];
          $empresa->cidade = $request->formulario['cidade'];
          $empresa->cep = $request->formulario['cep'];
          $empresa->save();
          return response()->json([
            'success'=>true,
            'mensagem'=>'Empresa salva com sucesso'
            ]);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function show(Empresas $empresas , Request $request)
    {
      $empresa = $empresas->find($request->id);
      return response()->json([
        'success'=>true,
        'mensagem'=>'Edicao id ='.$request->id,
        'empresa'=>$empresa
      ],200);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresas $empresas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresas $empresas)
    {
        $empresaSelecionada = $empresas->find($request->formulario['id']);

        if( $empresaSelecionada->razao_social == $request->formulario['razao_social'] ){
          //$empresaSelecionada->razao_social = $request->formulario['razao_social'];
          $empresaSelecionada->fantasia = $request->formulario['fantasia'];
          $empresaSelecionada->proprietario = $request->formulario['proprietario'];
          $empresaSelecionada->email = $request->formulario['email'];
          $empresaSelecionada->endereco = $request->formulario['endereco'];
          $empresaSelecionada->telefone = $request->formulario['telefone'];
          $empresaSelecionada->cidade = $request->formulario['cidade'];
          $empresaSelecionada->cep = $request->formulario['cep'];
          $empresaSelecionada->save();
          return response()->json([
            'success'=>true,
            'mensagem'=>'Edicao id ='.$request->id,

          ],200);

        }
        else{
          $verificarEmpresaExiste = $empresas::where('razao_social', '=' ,$request->formulario['razao_social'])->first();
          if($verificarEmpresaExiste){
            return response()->json([
              'success'=>false,
              'mensagem'=>'Empresa já existe , escolha outro nome'
            ]);
          }else{
            $empresaSelecionada->razao_social = $request->formulario['razao_social'];
            $empresaSelecionada->fantasia = $request->formulario['fantasia'];
            $empresaSelecionada->proprietario = $request->formulario['proprietario'];
            $empresaSelecionada->email = $request->formulario['email'];
            $empresaSelecionada->endereco = $request->formulario['endereco'];
            $empresaSelecionada->telefone = $request->formulario['telefone'];
            $empresaSelecionada->cidade = $request->formulario['cidade'];
            $empresaSelecionada->cep = $request->formulario['cep'];
            $empresaSelecionada->save();
            return response()->json([
              'success'=>true,
              'mensagem'=>'Edicao id ='.$request->id,

            ],200);
          }
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresas $empresas , Request $request)
    {

      $empresaSelecionada = $empresas::find($request->id);
      $empresaSelecionada->delete();
      return response()->json('success',200);

      //return response()->json('success',200);
    }
}
