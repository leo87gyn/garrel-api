<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venda;
use PDF;

class VendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $venda = new Venda();
      $todasAsVendas = Venda::with(['produtosVenda.fornecedor','produtosVenda.produto','user'])
      ->orderBy('id','DESC')
      ->paginate(5);
      return response()->json($todasAsVendas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $venda = new Venda();
        $vendaSelecionada = $venda::find($request->venda_id);
        $vendaSelecionada->delete();
        return response()->json('success',200);
    }

    public function gerarPdf(Request $request){
      $id_venda = $request->venda_id;
      $venda = new Venda();
      $chave = Venda::with(['produtosVenda','produtosVenda.produto','user','formas_pagamento'])
      ->where('id',$id_venda)
      ->first();
      //return response()->json($chave->formas_pagamento->nome);


      $email="belini.repre@gmail.com";
      $vendedor="Paulo César";
      $cliente_nome=$chave->cliente_nome;
      $data_venda = $chave->created_at;
      $data_venda = date('d-m-Y h:i:s', strtotime($data_venda));
      $total_geral = $chave->total;
      $endereco = $chave->endereco;
      $uuid = $chave->id;

      if($request->via=='1'){
        $via="<h1 style='color:red'>1º Via <br/>Empresa</h1>";
      }
      else{
        $via="<h1 style='color:red'>2º Via <br/>Cliente</h1>";
      }


      $html="<style type='text/css' media='all'>
        .center{
          text-align:center
        }
        .fonte{
          font-size:10px;
          font-family: Arial, Helvetica, sans-serif;
        }
        p{
          margin: 0;
          padding: 0;
        }
        table, td, th {
            border: 2px solid #000;
            text-align: left;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding-left: 15px;
            padding-right: 15px;
            padding-top:0px;
            padding-bottom:0px;
        }
        tr.noBorder td {
          border: 0;
        }


      </style>
        ";

      $html.="<table width='100%' border='2' >
      <thead  align='center' style='display: table-header-group'>
        <tr>
          <th class='center fonte' width='50%' >
               Descrição do produto
          </th>
          <th class='center fonte' >
              Quantidade
          </th>
          <th class='center fonte' >
              Preco(Kg)
          </th>
          <th class='center fonte' >
            <span>  Total </span>
          </th>
        </tr>
        </thead>
        ";
        //return response()->json($chave->produtosVenda);

        foreach ($chave->produtosVenda as $produto) {
          $html.='<tr style="text-align:center"><td class="fonte2">'.$produto->produto->nome.'</td>
          <td  class="fonte2" style="text-align:center" >'.$produto->quantidade.'</td>
          <td  class="fonte2"  style="text-align:center">'.str_replace(".",",",number_format($produto->preco,2)).'</td>
          <td  class="fonte2" style="text-align:center">'.str_replace(".",",",number_format($produto->quantidade*$produto->preco,2)).'</td>
          </tr>';
        }

        $html.='</table>
        <div style="padding-top:70px">


            <p>_______________________________________________
            <p>
            Assinatura do cliente
            </p>
        </div>

        <div style="float:right;margin-top:-300px;">

        <p style="border:2px solid #000;position:relative;left:-3px;padding:5px ">
          <b>R$ '.str_replace(".",",",number_format($total_geral,2)).'</b>
        </p>

        </div>
        ';






        $pdf = PDF::loadHTML('

        <div ><img style="margin-top:-10px" width="20%" src="http://www.garrel.com.br/img/logo-garrel.jpeg"></div>
        <div style="float:left;position:absolute;top:-53px;margin-left:25%">

            <h2 class="left" style="font-size:23px;padding-top:20px"> Garrel Comércio e Distribuição de Alimentos Ltda. </h2>
            <table width="100%" border="2" style="margin-top:-20px">
              <thead align="center" style="display: table-header-group">
              <tr>
                <td>
            <p class=" center fonte"><b> Telefone:</b>62) 3926-7778</p>
            <p class=" center fonte"><b>Email: </b>'.$email.'  </p>
            <p class=" center fonte"><b>Endereço: </b>Rua Benjamin Constant , </p>
            <p class=" center fonte"> N°176 QD-06 LT-09 , Jardim da Luz  </p>
            <p class=" center fonte"><b>Código:</b>
               1'.str_pad($uuid, 5, '0', STR_PAD_LEFT).'
            </p>
                </td>
                <td >
                  <p class="center fonte" ><b>'.$via.'</b><p>
                </td>
              </tr>
            </thead>

            </table>
        </div>

        <div style="">
        <p class="center" style=""> PEDIDO SEM VALOR FISCAL </p>

          <table width="100%">
            <thead  align="center" style="display: table-header-group">
            <tr>
            <td width="65%">
            <b  class="center fonte" >Cliente</b>:<span class="center" style="font-size:12px">'.$cliente_nome.'</span>
            </td>
              <td  >
              <b  class="center fonte">Data da Venda</b>:<span class="center" style="font-size:12px">'.$data_venda.'</span>
              </td>

            </tr>

            <tr>
            <td width="65%">
            <b  class="center fonte" >CNPJ/CPF</b>:<span class="center" style="font-size:12px">'.$chave->cnpj.'</span>
            </td>
              <td  >
              <b  class="center fonte">Pagamento</b>:<span class="center" style="font-size:12px">'.$chave->formas_pagamento->nome.'</span>
              </td>

            </tr>

            <tr>
              <td>
              <b  class="center fonte">Endereco</b>:<span class="center" style="font-size:12px">'.$endereco.'</span>
              </td>
              <td >
              <b  class="center fonte">Vendedor</b>:<span class="center" style="font-size:12px">'.$vendedor.'</span>
              </td>
            </tr>
            </thead>

          </table>

        </div>

        <div>



            '.$html.'




        </div>





        ');
        $pdf->setPaper('A4');
        //$pdf->s();
        //$output = $pdf->output();
        //return file_put_contents('Brochure.pdf', $pdf->stream());
        return $pdf->stream();

    }
}
