<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosVenda extends Model
{
    //
    public function fornecedor(){
      return $this->belongsTo('App\Empresas','fornecedor_id');
    }
    public function produto(){
      return $this->belongsTo('App\Produto','produto_id');
    }
}
