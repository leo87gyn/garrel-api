<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    public function produtosVenda(){
      return $this->hasMany('App\ProdutosVenda');
    }
    public function user(){
      return $this->belongsTo('App\User');
    }
    public function formas_pagamento(){
      return $this->belongsTo('App\FormasDePagamento','tipo_pagamento_id');
    }

}
