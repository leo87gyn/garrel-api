<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutosErp extends Model
{
  /*
    Listagem resumida de produtos
  */
    public function mostrarProdutosErp(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://app.omie.com.br/api/v1/geral/produtos/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ListarProdutosResumido\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"pagina\":1,\n\t\t\t\"registros_por_pagina\":100,\n\t\t\t\"apenas_importado_api\":\"N\",\n\t\t\t\"filtrar_apenas_omiepdv\": \"N\"\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: 1a442489-1bf4-4635-856b-dc6143ec759b",
          "apenas_importado_api: N",
          "app_key: 480492519507",
          "app_secret: 8d3aa8cf9538a9179193ad4a654efb09",
          "call: ListarProdutosResumido",
          "pagina: 1",
          "registros_por_pagina: 100"
        ),
      ));

      $response = curl_exec($curl);

      $curlPaginaDois = curl_init();

      curl_setopt_array($curlPaginaDois, array(
        CURLOPT_URL => "https://app.omie.com.br/api/v1/geral/produtos/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ListarProdutosResumido\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"pagina\":2,\n\t\t\t\"registros_por_pagina\":100,\n\t\t\t\"apenas_importado_api\":\"N\",\n\t\t\t\"filtrar_apenas_omiepdv\": \"N\"\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: 1a442489-1bf4-4635-856b-dc6143ec759b",
          "apenas_importado_api: N",
          "app_key: 480492519507",
          "app_secret: 8d3aa8cf9538a9179193ad4a654efb09",
          "call: ListarProdutosResumido",
          "pagina: 1",
          "registros_por_pagina: 100"
        ),
      ));

      $responsePaginaDois = curl_exec($curlPaginaDois);


      $err = curl_error($curl);
      $errPaginaDois = curl_error($curlPaginaDois);

      curl_close($curl);
      curl_close($curlPaginaDois);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        //echo  $response ;
        $json = json_decode($response, true);
        $jsonPaginaDois = json_decode($responsePaginaDois, true);
        $result = array_merge($json['produto_servico_resumido'],$jsonPaginaDois['produto_servico_resumido']);
        $json['produto_servico_resumido'] = $result;

        return $json;
        }
    }

    public function retornaProdutoPorCodigo($codigo){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://app.omie.com.br/api/v1/geral/produtos/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ConsultarProduto\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"codigo_produto\":\"$codigo\",\n\t\t\t\"codigo_produto_integracao\":\"\",\n\t\t\t\"codigo\":\"PRC00002\"\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: 66a553af-1e91-4277-af1c-17e23561c42a",
          "apenas_importado_api: N",
          "app_key: 480492519507",
          "app_secret: 8d3aa8cf9538a9179193ad4a654efb09",
          "call: ListarClientes",
          "pagina: 1",
          "registros_por_pagina: 50"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        //echo  $response ;
        $json = json_decode($response, true);
        return $json['descricao'];
        }
    }
}
