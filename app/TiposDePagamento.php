<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposDePagamento extends Model
{
    public function mostrarFormasDePagamento($pagina){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://app.omie.com.br/api/v1/produtos/formaspagcompras/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\n\t\"call\":\"ListarFormasPagCompras\",\n\t\"app_key\":\"480492519507\",\n\t\"app_secret\":\"8d3aa8cf9538a9179193ad4a654efb09\",\n\t\"param\":[\n\t\t{\n\t\t\t\"pagina\": $pagina,\n\t\t\t\"registros_por_pagina\": 50\n\t\t\t\n\t\t}\n\t\t]\n\t\n}",
        CURLOPT_HTTPHEADER => array(
          "Cache-Control: no-cache",
          "Content-Type: application/json",
          "Postman-Token: 65aac6bb-37a2-418b-afdc-a9f0b24ad433",
          "apenas_importado_api: N",
          "app_key: 480492519507",
          "app_secret: 8d3aa8cf9538a9179193ad4a654efb09",
          "call: ListarClientes",
          "pagina: 1",
          "registros_por_pagina: 50"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $json = json_decode($response, true);
        return $json;
      }
    }
}
