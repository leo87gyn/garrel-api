<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produto extends Model
{
    use SoftDeletes;

    //protected $table="produtos";

    public function categoria_id(){
      return $this->belongsTo('App\Categoria',"categorias_id");
    }
}
