<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosVendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos_vendas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('venda_id')->unsigned();
          $table->foreign('venda_id')
                ->references('id')
                ->on('vendas')
                ->onDelete('cascade');
          $table->integer('produto_omie_id')->unsigned();
          $table->string('produto_nome');
          $table->decimal('preco', 8, 2);
          $table->integer('quantidade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos_vendas');
    }
}
