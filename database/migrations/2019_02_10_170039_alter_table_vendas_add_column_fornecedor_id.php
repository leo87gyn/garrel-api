<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendasAddColumnFornecedorId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasColumn('vendas', 'tipo_pagamento_id'))
      {
      Schema::table('vendas', function (Blueprint $table) {
       $table->integer('tipo_pagamento_id')->unsigned();
       $table->foreign('tipo_pagamento_id')->references('id')->on('formas_de_pagamentos');
     });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
