<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProdutosVendasAddColumnFornecedorId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasColumn('produtos_vendas', 'fornecedor_id'))
        {
          Schema::table('produtos_vendas', function (Blueprint $table) {
           $table->integer('fornecedor_id')->unsigned();
           $table->foreign('fornecedor_id')->references('id')->on('empresas');
         });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
